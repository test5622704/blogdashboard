import React, { useState } from "react";
import Navbar from "./Navbar";
import Footer from "./Footer";
import Post from "./Post";

function App() {
  const [posts, setPosts] = useState([
    {
      id: 1,
      username: "John",
      message:"What is Motivation? ",
      timestamp: new Date(),
      isEditing: false,
      editedMessage: "",
    },
    // Weitere Posts hier...
     {
      id: 2,
      username: "Alina",
      message: "“People Often Say That Motivation Doesn’t Last. Well, Neither Does Bathing – That’s Why We Recommend It Daily.”",
      timestamp: new Date(),
      isEditing: false,
      editedMessage: "",
    },
    {
      id: 2,
      username: "Zhyldyz",
      message: "How to master your time: https://oliveremberton.com/2013/how-to-master-your-time/",
      timestamp: new Date(),
      isEditing: false,
      editedMessage: "",
      
    },
    {
      id: 3,
      username: "Diana",
      message: "How to master your time: https://oliveremberton.com/2013/how-to-master-your-time/",
      timestamp: new Date(),
      isEditing: false,
      editedMessage: "",
      
    },
  ]);

  const handleEdit = (postId) => {
    const updatedPosts = posts.map((post) => {
      if (post.id === postId) {
        return { ...post, isEditing: true };
      }
      return post;
    });
    setPosts(updatedPosts);
  };

  const handleSave = (postId) => {
    const updatedPosts = posts.map((post) => {
      if (post.id === postId) {
        return {
          ...post,
          isEditing: false,
          message: post.editedMessage || post.message,
          editedMessage: "",
        };
      }
      return post;
    });
    setPosts(updatedPosts);
  };

  const handleInputChange = (postId, value) => {
    const updatedPosts = posts.map((post) => {
      if (post.id === postId) {
        return { ...post, editedMessage: value };
      }
      return post;
    });
    setPosts(updatedPosts);
  };

  return (
    <div>
      <Navbar />
      <h1>Welcome to my blog website!</h1>
      <div className="posts">
        {posts.map((post) => (
          <div key={post.id}>
            <Post
              username={post.username}
              message={post.isEditing ? (
                <textarea
                  value={post.editedMessage || post.message}
                  onChange={(e) => handleInputChange(post.id, e.target.value)}
                />
              ) : post.message}
              timestamp={post.timestamp}
            />
            {post.isEditing ? (
              <button onClick={() => handleSave(post.id)}>Save</button>
            ) : (
              <button onClick={() => handleEdit(post.id)}>Edit</button>
            )}
          </div>
        ))}
      </div>
      <Footer />
    </div>
  );
}

export default App;
